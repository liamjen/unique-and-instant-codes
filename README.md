# Unique and Instantaneous Codes
## Dependencies
* python3

## Usage
* Clone this repository `git clone https://gitlab.com/liamjen/unique-and-instant-codes.git`
* Within the cloned directory, run `python solution.py` or `python3 solution.py`
* You should see the following prompt:

```
[1] Unique Decodability
[2] Instant Code
[Q] Quit
Enter a selection: 
```
* Press `1 [Enter]` to check of a code is uniquely decodable. (See __About: Uniquely Decodable Codes__)
* Press `2 [Enter]` to check of a code with given symbol lengths is instantaneous. (See __About: Instantaneous Codes__)
* Press `q [Enter]` to exit

## About

### Uniquely Decodable Codes
* Enter a set of code symbols separated by commas (E.g.: `02, 12, 21, 121`)
* The program will print if the code is uniquely decodable (no symbol can be mistaken for another)

#### Example

```
0, 12, 121, 02
-------------------------------------------------
C-0: {'02', '0', '12', '121'}
C-1: {'2', '1'}
C-2: {'2', '21'}
Code {'02', '0', '12', '121'} is unique
```

### Instantaneous Codes
* Enter a list of code symbol lengths (E.g.: `1, 2, 3, 3, 4`)
* Next, enter the r-ary base the (`2` = binary, `3` = ternary, etc)
* The program will state if an instantaneously decodable code can be created, and print the code if it can

#### Example

```
1, 2, 3, 3, 4 
Enter a r-ary base (E.g.: 2)
3
-------------------------------------------------
Is instant code
Code: {'111', '110', '1120', '0', '10'}
```

