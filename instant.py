
class Inst():
    """
    Returns true if given code_lengths and r-ary base can build an instant code from book definition

    if sum of 1/base^(word_length) > 1 the code is not instant
    """
    def is_instant(code_lengths, base=2):
        sum_prop = 0

        for l in code_lengths:
            sum_prop += (1 / pow(base, l))

        if sum_prop > 1:
            return False
        return True

    """
    Returns instantaneous codes from given code_lengths and r-ary base.

    Call is_instant() first to see if the code can be created.
    """
    def get_inst_code(code_lengths, base=2):
        tree = Tree()
        code_lengths = sorted(code_lengths)
        max_len = code_lengths[-1]

        tree.build(code_lengths, base, max_len)
        codes = tree.traverse(code_lengths)

        return codes

class Tree():
    """
    Basic tree structure for creating r-ary trees from codes to make instantaneous codes
    """
    def __init__(self):
        self.root = Node(None, None)

    """
    Builds the tree structure based on the given code_lengths. Tree will have nodes with base number of children
    """
    def build(self, code_lengths, base, max_len):
        Tree.build_recurse(self.root, code_lengths, base, max_len)

    """
    Builds the tree structure recursively from node
    """
    def build_recurse(node, code_lengths, base, level):
        if level == 0:
            return
        else:
            Tree.init_children(node, base)

            for i in range(len(node.children)):
                child = node.children[i]
                Tree.build_recurse(child, code_lengths, base, level - 1)

    """
    Traverses the tree recursively to find codes from given code_lengths
    """
    def traverse(self, code_lengths):
        codes = Tree.traverse_recurse(self.root, code_lengths, 0)
        return codes

    """
    Recursively traverses from node to build code set from given code_lengths

    level: depth in tree
    """
    def traverse_recurse(node, code_lengths, level):
        codes = set()
        if node is None or len(code_lengths) == 0:
            return codes

        if level == code_lengths[0]:
            codes.add(Tree.get_string(node))
            node.mark = True
            del(code_lengths[0])
            for c in node.parent.children:
                if not c.mark:
                    codes = codes.union(Tree.traverse_recurse(c, code_lengths, level))

        for c in node.children:
            if not c.mark:
                codes = codes.union(Tree.traverse_recurse(c, code_lengths, level + 1))

        return codes

    """
    Follows backwards from node (parents) and builds string based on the node's data
    """
    def get_string(node):
        string = ""
        string += str(node.data)
        node = node.parent
        while node != None and node.data != None:
            s = str(node.data)
            string = s + string
            node = node.parent
        return string
            

    """
    Give node base number of children
    """
    def init_children(node, base):
        node.children = [Node(i, node) for i in range(base)]


class Node():
    """
    Generic node for tree structure

    data: partial symbol data
    parent: parent node
    children: list of child nodes
    mark: optional boolean for node traversial
    """
    def __init__(self, data, parent, children=[], mark=False):
        self.data = data
        self.parent = parent
        self.children = children
        self.mark = mark

    def __repr__(self):
        return f"data: {self.data}, children: {len(self.children)}"



