
class UniquelyDecodable():
    """
    Returns True if code is uniquely decodable (and C-infinity) or False if not (and empty set)
    """
    def is_uniquely_decodable(code):
        inf = UniquelyDecodable.code_seq_inf(code)
        if len(code.intersection(inf)) > 0:
            return (False, inf)
        else:
            return (True, {})

    """
    Returns C-infinity from code set.
    """
    def code_seq_inf(code):
        c_inf = UniquelyDecodable.code_seq_recurse(code, code, 0)
        return c_inf

    """
    Recursively calculates C-infinity from code set.

    prev: Set of previous C-[num]
    depth: Recursion depth
    """
    def code_seq_recurse(code, prev, depth):
        next_seq = set()
        
        print(f"C-{depth}: {prev}")

        # Stop if the sets are looping
        if len(prev) == 0 or depth > 75:
            return next_seq

        for c in code:
            for p in prev:
                u, v = (p, c) if len(p) < len(c) else (c, p)        # Smaller string from code or previous C-[num]
                if v.find(u) == 0 and len(v) != len(u):
                    rest = v[len(u):]                               # Composite: u + rest = v
                    if (u in c and v in p) or (u in p and v in c):  # From definition: Part of C-[num] if code or previous C contains symbols
                        next_seq.add(rest)


        # Stop if no more are found, or same results found
        if len(next_seq) == 0 or next_seq == prev:
            return next_seq

        return next_seq.union(UniquelyDecodable.code_seq_recurse(code, next_seq, depth + 1))

                    



