# Author: Liam Jensen

from uniquedecode import UniquelyDecodable
from instant import Inst

# See uniquedecode.py and instant.py for implemented solutions.
# Below are helper functions for taking user input via CLI.

def printline():
    print("-------------------------------------------------")


def unique_decodability():
    while True:
        user_in = input("\nEnter codes separated by commas. (E.g.: 0, 12, 21, 121, 02)\n")
        if user_in == "q": return
        printline()

        codes = set(user_in.replace(" ", "").split(","))
        is_uniq, c_inf = UniquelyDecodable.is_uniquely_decodable(codes)
        if is_uniq:
            print(f"Code {codes} is unique\n")
        else:
            print(f"\nCode {codes}\nC-inf {c_inf}\nIntersect {codes.intersection(c_inf)}\nCode is not unique\n")
        
    
def instant_code():
    while True:
        user_in_lens = input("\nEnter lengths of codes separated by commas. (E.g.: 1, 2, 4, 4)\n")
        if user_in_lens == "q": return
        user_in_base = input("Enter a r-ary base (E.g.: 2)\n")
        if user_in_base == "q": return
        printline()

        lens = user_in_lens.replace(" ", "").split(",")
        lens = [int(i) for i in lens]
        base = int(user_in_base.replace(" ", ""))
        is_inst = Inst.is_instant(lens, base=base)
        if is_inst:
            inst_code = Inst.get_inst_code(lens, base=base)
            print(f"Is instant code\nCode: {inst_code}")
        else:
            print("Not instant")
        


if __name__ == "__main__":
    while True:
        user_in = input("[1] Unique Decodability\n[2] Instant Code\n[Q] Quit\nEnter a selection: ")
        if user_in == "1":
            unique_decodability()
        elif user_in == "2":
            instant_code()
        elif user_in == "q":
            exit(0)
        printline()
        
    



